<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Simple_One
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'simpleone' ); ?></a>

	<header id="masthead" class="site-header">
	
		<div class="login-bar">
			<spna class="login-bar__custom-content">Custom content</spna>
			<span class="login-bar__register">Login/Signin</span>			
		</div>
		<!-- <figure class="site-header-image">
		<?php //the_header_image_tag(); ?>	
		</figure> -->
		<div style="background-image: url(<?php header_image(  ); ?>); opacity: <?php echo get_theme_mod( 'header_image_opacity'); ?>;" class="site-branding">
		
			<div class="logo"> 
			<?php
			if (has_custom_logo( ))  { the_custom_logo( );}
			else {echo '';}?>
			</div><!-- logo -->
			<div class="title">
			<?php
			if ( is_front_page() || is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<h2 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h2>
				<?php
			endif;
			$simpleone_description = get_bloginfo( 'description', 'display' );
			if ( $simpleone_description || is_customize_preview() ) :
				?>
				<h3 class="site-description"><?php echo $simpleone_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></h3>
			<?php endif; ?>
			</div><!-- title -->
			<div class="theerd"></div>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'simpleone' ); ?></button>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				)
			);
			?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
