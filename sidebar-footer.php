<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Simple_One
 */

if ( ! is_active_sidebar( 'footer-bar' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area footer-widget">
	<?php dynamic_sidebar( 'footer-bar' ); ?>
</aside><!-- #secondary -->
