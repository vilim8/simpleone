<?php

/**
 * Simple One Theme Customizer
 *
 * @package Simple_One
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function simpleone_customize_register($wp_customize)
{
	$wp_customize->get_setting('blogname')->transport         = 'postMessage';
	$wp_customize->get_setting('blogdescription')->transport  = 'postMessage';
	$wp_customize->get_setting('header_textcolor')->transport = 'postMessage';

	if (isset($wp_customize->selective_refresh)) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'simpleone_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'simpleone_customize_partial_blogdescription',
			)
		);
	}
// Add custom options for post display content or excerpt
	$wp_customize->add_section('theme_options', array(
		'title' => __('Theme Options', 'simpleone'),
		//'description' => __('Selecet theme options here'),
		'priority' => 100,
	));

	$wp_customize->add_setting('lenght_setting', array(
		'default' => 'excerpt',
		'type'	  => 'theme_mod',
		'sanitize_callback'	=> 'simpleone_santitize_length',
		'transport'		=>	'postMessage',
	));

	$wp_customize->add_control('simpleone_length_control', array(
		'type'	=> 'radio',
		'label' => __('index/archive displays', 'simpleone'),
		'section' => 'theme_options',
		'choices' => array(
			'excerpt' => __('Excerpt (default)', 'simpleone'),
			'full-content'	=> __('Full content', 'simpleone'),
					),
		'settings'		 => 'lenght_setting'
		));
//  Add custom background clolor for header
	$wp_customize->add_setting('header_bg_color', array(
		'default'	=>	'#dfe3ab',
		'transport'	=> 	'postMessage',
		'type'		=>	'theme_mod',
		'sanitize_callback' => 'sanitize_hex_color_no_hash'
	));
	$wp_customize->add_control(new WP_Customize_Color_Control(
		$wp_customize,
		'header_bg_color' , array(
			'label'	=> __('Header Bacground color', 'simpleone'),
			'section'	=> 'theme_options',
			'settings'	=> 'header_bg_color'
		)
		));		
	//  Add custom background color for footer
    $wp_customize->add_setting('footer_bg_color', array(
		'default'	=>	'#000',
		'transport'	=> 'postMessage',
		'type'		=> 'theme_mod',
		'sanitize_callback'	=> 'sanitize_hex_color_no_hash',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control(
		$wp_customize,
		'theme_bg_color', array(
			'label'	=>	__('Footer background color', 'simpleone'),
			'section' => 'theme_options',
			'settings'	=> 'footer_bg_color'
		)
	));

	//  Add custom copyright
	$wp_customize->add_setting('footer_copyright', array(
	'default'	=>	'&copy; ' . get_bloginfo( 'name' )
	));
	$wp_customize->add_control('footer_copyright', array(
		'label'	=>	'Copyright',
		'type'	=>	'text',
		'section'	=>	'theme_options'
	));

	//Add range (slider) control for header image opacity
	$wp_customize->add_setting('header_image_opacity', array(
		'default'	=>	'1',
	));
	$wp_customize->add_control('header_image_opacity', array(
		'type'	=>	'range',
		'label' =>	__('Header image opacity', 'simpleone'),
		'section' =>	'header_image',
		'input_attrs'	=> array(
			'min'	=>	0,
			'max'	=>	1,
			'step'	=>  0.1,
		),
		'priority'	=> 50,
		'transport'	=>	'postMessage',
	));

	$wp_customize->add_setting('header_image_opacity2', array(
		'default'	=>	'1',
	));
    $wp_customize->add_control('header_image_opacity2', array(
		'type'	=>	'text',
		'section' =>	'header_image',
		'priority'	=> 55,
	));
}
add_action('customize_register', 'simpleone_customize_register');

function simpleone_santitize_length($value) {
	if( ! in_array($value, array( 'excerpt', 'full-content')) ){
		$value = 'excerpt';
	}
	return $value;
}

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function simpleone_customize_partial_blogname()
{
	bloginfo('name');
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function simpleone_customize_partial_blogdescription()
{
	bloginfo('description');
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function simpleone_customize_preview_js()
{
	wp_enqueue_script('simpleone-customizer', get_template_directory_uri() . '/js/customizer.js', array('customize-preview'), '20151215', true);
}
add_action('customize_preview_init', 'simpleone_customize_preview_js');





if ( ! function_exists( 'simpleone_header_style' ) ) :
	/**
	 * Styles the header image and text displayed on the blog.
	 *
	 * @see simpleone_custom_header_setup().
	 */
	function simpleone_header_style() {
		$header_text_color = get_header_textcolor();
		
		

		/*
		 * If no custom options for text are set, let's bail.
		 * get_header_textcolor() options: Any hex value, 'blank' to hide text. Default: add_theme_support( 'custom-header' ).
		 */
		if ( get_theme_support( 'custom-header', 'default-text-color' ) === $header_text_color ) {
			return;
		}

		// If we get this far, we have custom styles. Let's do this.
		?>
		<style type="text/css">
		<?php
		// Has the text been hidden?
		if ( ! display_header_text() ) :
			?>
			.site-title,
			.site-description {
				position: absolute;
				clip: rect(1px, 1px, 1px, 1px);
				}
			<?php
			// If the user has set a custom color for the text use that.
		else :
			?>
			.site-title a,
			.site-description {
				color: #<?php echo esc_attr( $header_text_color ); ?>;
			}
		<?php endif; ?>
		</style>
		<?php
	}




	

endif;

	/*
	*	Is custom header background setit
	*/
	function simpleone_header_bg_color(){
		$header_bg_color = get_theme_mod( 'header_bg_color' );

			if ('#000' != $header_bg_color) { ?>
		<style type="text/css">
		.site-header{
			background-color: #<?php echo esc_attr( $header_bg_color ); ?>;
			}
		</style>
		<?php	
		}	
	
	}
	add_action( 'wp_head', 'simpleone_header_bg_color' );

		/*
	*  Is custom footer background setit
	*/
	function simpleone_footer_bg_color(){
		$footer_bg_color = get_theme_mod( 'footer_bg_color' );
			if ('#000' != $footer_bg_color)	{ ?>
		<style type="text/css">
		.site-footer{
			background-color: #<?php echo esc_attr( $footer_bg_color ); ?>;
		}</style>

		<?php
		}
	}
	add_action( 'wp_head', 'simpleone_footer_bg_color' );
