<?php
/**
 * Sample implementation of the Custom Header feature
 *
 * You can add an optional custom header image to header.php like so ...
 *
	<?php the_header_image_tag(); ?>
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-headers/
 *
 * @package Simple_One
 */

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses simpleone_header_style()
 */
function simpleone_custom_header_setup() {
	add_theme_support(
		'custom-header',
		apply_filters(
			'simpleone_custom_header_args',
			array(
				'default-image'      => '',
				'default-text-color' => '000000',
				'width'              => 2000,
				'height'             => 296,
				'flex-height'        => true,
				'wp-head-callback'   => 'simpleone_header_style',
			)
		)
	);
}
add_action( 'after_setup_theme', 'simpleone_custom_header_setup' );

